<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>CreateRoom</name>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="51"/>
        <source>Create Room</source>
        <translation>Skapa rum</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="52"/>
        <source>Create</source>
        <translation>Skapa</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="57"/>
        <source>Visibility</source>
        <translation>Synlighet</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="59"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="60"/>
        <source>Public</source>
        <translation>Offentlig</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="71"/>
        <location filename="../qml/pages/CreateRoom.qml" line="72"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="80"/>
        <source>Topic</source>
        <translation>Rubrik</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="81"/>
        <source>Topic (optional)</source>
        <translation>Rubrik (frivilligt)</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="102"/>
        <location filename="../qml/pages/CreateRoom.qml" line="103"/>
        <source>Room alias</source>
        <translation>Rumsalias</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="120"/>
        <source>Enable end-to-end encryption</source>
        <translation>Aktivera kryptering från slutpunkt till slutpunkt</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="121"/>
        <source>Bridges and most bots won&apos;t work yet.</source>
        <translation>Bryggor och de flesta bottar kommer ännu inte att fungera.</translation>
    </message>
</context>
<context>
    <name>Credits</name>
    <message>
        <location filename="../qml/pages/Credits.qml" line="10"/>
        <source>Credits</source>
        <translation>Tack</translation>
    </message>
</context>
<context>
    <name>ErrorPage</name>
    <message>
        <location filename="../qml/pages/ErrorPage.qml" line="12"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <location filename="../qml/pages/Invite.qml" line="15"/>
        <source>Invite</source>
        <translation>Bjud in</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source>You&apos;ve been invited to</source>
        <translation>Du har inviterats till</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source> chat with</source>
        <translation> att chatta med</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="51"/>
        <source>Join</source>
        <translation>Anslut</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="65"/>
        <source>Reject</source>
        <translation>Avvisa</translation>
    </message>
</context>
<context>
    <name>JoinPublicRoom</name>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="19"/>
        <source>Join Room</source>
        <translation>Anslut rum</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="20"/>
        <source>Join</source>
        <translation>Anslut</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="78"/>
        <source> members</source>
        <translation> medlemmar</translation>
    </message>
</context>
<context>
    <name>LoggedInCover</name>
    <message>
        <location filename="../qml/cover/LoggedInCover.qml" line="43"/>
        <source> notifications</source>
        <translation> aviseringar</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="34"/>
        <source>Login</source>
        <translation>Logga in</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="38"/>
        <source>Homeserver</source>
        <translation>Hemserver</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="39"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation>Hemserver-URL (matrix.org)</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="48"/>
        <source>Username</source>
        <translation>Användarnamn</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="49"/>
        <source>Username (user1)</source>
        <translation>Användarnamn (användare1)</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="58"/>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="73"/>
        <source>Device display name</source>
        <translation>Enhetens visningsnamn</translation>
    </message>
</context>
<context>
    <name>MessageSource</name>
    <message>
        <location filename="../qml/pages/MessageSource.qml" line="16"/>
        <source>View Source</source>
        <translation>Visa källa</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../qml/pages/Messages.qml" line="38"/>
        <location filename="../qml/pages/Messages.qml" line="41"/>
        <source>File saved</source>
        <translation>Fil sparad</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="39"/>
        <source>File saved to Downloads directory</source>
        <translation>Fil sparad i nerladdningsmappen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="137"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="143"/>
        <source>Reply</source>
        <translation>Svara</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="146"/>
        <source>Replying to </source>
        <translation>Svarar till </translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="159"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="177"/>
        <source>View Source</source>
        <translation>Visa källa</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="182"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="276"/>
        <source>Download audio</source>
        <translation>Ladda ner ljud</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="308"/>
        <location filename="../qml/pages/Messages.qml" line="328"/>
        <source>View video</source>
        <translation>Visa video</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="308"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="337"/>
        <source>Download file</source>
        <translation>Ladda ner fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="430"/>
        <source>Message</source>
        <translation>Meddelande</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="431"/>
        <source>Send message to room</source>
        <translation>Skicka meddelande till rum</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="461"/>
        <source>New Messages</source>
        <translation>Nya meddelanden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="482"/>
        <source>Send file</source>
        <translation>Skicka fil</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1294"/>
        <source> invited </source>
        <translation> inbjuden </translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1298"/>
        <source> changed their profile</source>
        <translation> ändrade sin profil</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1300"/>
        <source> joined</source>
        <translation> anslöt</translation>
    </message>
</context>
<context>
    <name>PictureDisplay</name>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="17"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="20"/>
        <source>Image saved</source>
        <translation>Bild sparad</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="18"/>
        <source>Image saved to Pictures directory</source>
        <translation>Bild sparad i bildmappen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="26"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="29"/>
        <source>Image error</source>
        <translation>Bildfel</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="27"/>
        <source>Image not saved to Pictures directory</source>
        <translation>Bilden sparades inte i bildmappen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="51"/>
        <source>Save to Pictures</source>
        <translation>Sparat i Bilder</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="59"/>
        <source>Share</source>
        <translation>Dela</translation>
    </message>
</context>
<context>
    <name>RoomDirectory</name>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="28"/>
        <source>Explore public rooms</source>
        <translation>Utforska offentliga rum</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="33"/>
        <source>Find a room...</source>
        <translation>Hitta ett rum...</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="46"/>
        <source>Search in</source>
        <translation>Sök i</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="49"/>
        <source>My homeserver</source>
        <translation>Min hemserver</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="50"/>
        <source>Matrix.org</source>
        <translation>Matrix.org</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="60"/>
        <source>No results</source>
        <translation>Inga träffar</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="61"/>
        <source>Please try a different search term</source>
        <translation>Försök med en annan sökterm</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="71"/>
        <source>Search for a room</source>
        <translation>Sök efter ett rum</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="72"/>
        <source>Use the search box to search for a room</source>
        <translation>Använd sökfältet för att söka efter ett rum</translation>
    </message>
</context>
<context>
    <name>Rooms</name>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="129"/>
        <source>No Favorites</source>
        <translation>Inga favoriter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="145"/>
        <source>No Rooms</source>
        <translation>Inga rum</translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="161"/>
        <source>No Direct Messages</source>
        <translation>Inga direktmeddelanden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="176"/>
        <source>No Invites</source>
        <translation>Inga inbjudningar</translation>
    </message>
</context>
<context>
    <name>RoomsDisplay</name>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="42"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="47"/>
        <source>Explore public rooms</source>
        <translation>Utforska offentliga rum</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="53"/>
        <source>Create new room</source>
        <translation>Skapa nytt rum</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="59"/>
        <location filename="../qml/custom/RoomsDisplay.qml" line="74"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="59"/>
        <source>Hide Search</source>
        <translation>Dölj sök</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="95"/>
        <source>Remove from Favorites</source>
        <translation>Ta bort från favoriter</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="95"/>
        <source>Favorite</source>
        <translation>Favorit</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="107"/>
        <source>Leave</source>
        <translation>Lämna</translation>
    </message>
</context>
<context>
    <name>SSOLogin</name>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="36"/>
        <source>Login with SSO</source>
        <translation>Logga in med SSO</translation>
    </message>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="40"/>
        <source>Homeserver</source>
        <translation>Hemserver</translation>
    </message>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="41"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation>Hemserver-URL (matrix.org)</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Notifications</source>
        <translation>Aviseringar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="65"/>
        <source>Enable notifications</source>
        <translation>Aktivera avisering</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="72"/>
        <source>Enable notifications when app is closed</source>
        <translation>Aktivera avisering när appen är stängd</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="80"/>
        <source>Notification checking interval</source>
        <translation>Aviseringens kontrollintervall</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <source>30 seconds</source>
        <translation>30 sekunder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="83"/>
        <source>2.5 minutes</source>
        <translation>2.5 minuter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="84"/>
        <source>5 minutes</source>
        <translation>5 minuter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <source>10 minutes</source>
        <translation>10 minuter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="86"/>
        <source>15 minutes</source>
        <translation>15 minuter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>30 minutes</source>
        <translation>30 minuter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>1 hour</source>
        <translation>1 timma</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="89"/>
        <source>2 hours</source>
        <translation>2 timmar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>4 hours</source>
        <translation>4 timmar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>8 hours</source>
        <translation>8 timmar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="92"/>
        <source>10 hours</source>
        <translation>10 timmar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="93"/>
        <source>12 hours</source>
        <translation>12 timmar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="94"/>
        <source>24 hours</source>
        <translation>24 timmar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="104"/>
        <source>Display</source>
        <translation>Visning</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="109"/>
        <source>Sort rooms by</source>
        <translation>Sortera rum efter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="111"/>
        <source>Activity</source>
        <translation>Aktivitet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Alphabetical</source>
        <translation>Alfabetet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="122"/>
        <source>Display user avatars</source>
        <translation>Visa användares avatarer</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="129"/>
        <source>Global</source>
        <translation>Övergripande</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="133"/>
        <source>Clear cache</source>
        <translation>Rensa cache</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="134"/>
        <source>Cleared cache</source>
        <translation>Rensade cache</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="138"/>
        <source>Logout</source>
        <translation>Logga ut</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="140"/>
        <source>Logging out</source>
        <translation>Loggar ut</translation>
    </message>
</context>
<context>
    <name>Share</name>
    <message>
        <location filename="../qml/pages/Share.qml" line="13"/>
        <source>Share</source>
        <translation>Dela</translation>
    </message>
</context>
<context>
    <name>Start</name>
    <message>
        <location filename="../qml/pages/Start.qml" line="22"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="40"/>
        <source>Welcome</source>
        <translation>Välkommen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="44"/>
        <source>Log in with Username/Password</source>
        <translation>Logga in med namn/lösenord</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="52"/>
        <source>Log in with SSO</source>
        <translation>Logga in med SSO</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../qml/pages/User.qml" line="83"/>
        <source>Direct Message</source>
        <translation>Direktmeddelande</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="94"/>
        <source>Ignore</source>
        <translation>Ignorera</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="104"/>
        <source>Unignore</source>
        <translation>Ignorera inte</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="17"/>
        <location filename="../qml/pages/VideoPage.qml" line="20"/>
        <source>Video saved</source>
        <translation>Video sparad</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="18"/>
        <source>Video saved to Videos directory</source>
        <translation>Video sparad i videomappen</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="26"/>
        <location filename="../qml/pages/VideoPage.qml" line="29"/>
        <source>Video error</source>
        <translation>Videofel</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="27"/>
        <source>Video not saved to Videos directory</source>
        <translation>Videon sparades inte i videomappen</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="57"/>
        <source>Share</source>
        <translation>Dela</translation>
    </message>
</context>
</TS>
