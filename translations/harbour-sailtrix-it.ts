<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>CreateRoom</name>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="51"/>
        <source>Create Room</source>
        <translation>Crea Stanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="52"/>
        <source>Create</source>
        <translation>Crea</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="57"/>
        <source>Visibility</source>
        <translation>Visibilità</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="59"/>
        <source>Private</source>
        <translation>Privata</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="60"/>
        <source>Public</source>
        <translation>Pubblica</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="71"/>
        <location filename="../qml/pages/CreateRoom.qml" line="72"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="80"/>
        <source>Topic</source>
        <translation>Argomento</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="81"/>
        <source>Topic (optional)</source>
        <translation>Argomento (facoltativo)</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="102"/>
        <location filename="../qml/pages/CreateRoom.qml" line="103"/>
        <source>Room alias</source>
        <translation>Alias stanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="120"/>
        <source>Enable end-to-end encryption</source>
        <translation>Abilita crittografia end-to-end</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="121"/>
        <source>Bridges and most bots won&apos;t work yet.</source>
        <translation>I ponti con altre piattaforme e la maggior parte dei bot ancora non funzionano.</translation>
    </message>
</context>
<context>
    <name>Credits</name>
    <message>
        <location filename="../qml/pages/Credits.qml" line="10"/>
        <source>Credits</source>
        <translation>Crediti</translation>
    </message>
</context>
<context>
    <name>ErrorPage</name>
    <message>
        <location filename="../qml/pages/ErrorPage.qml" line="12"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <location filename="../qml/pages/Invite.qml" line="15"/>
        <source>Invite</source>
        <translation>Invita</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source>You&apos;ve been invited to</source>
        <translation>Sei stato invitato su</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source> chat with</source>
        <translation>Scrivi a</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="51"/>
        <source>Join</source>
        <translation>Entra</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="65"/>
        <source>Reject</source>
        <translation>Respingi</translation>
    </message>
</context>
<context>
    <name>JoinPublicRoom</name>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="19"/>
        <source>Join Room</source>
        <translation>Entra nella Stanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="20"/>
        <source>Join</source>
        <translation>Entra</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="78"/>
        <source> members</source>
        <translation>membri</translation>
    </message>
</context>
<context>
    <name>LoggedInCover</name>
    <message>
        <location filename="../qml/cover/LoggedInCover.qml" line="43"/>
        <source> notifications</source>
        <translation>notifiche</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="34"/>
        <source>Login</source>
        <translation>Effettua l&apos;accesso</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="38"/>
        <source>Homeserver</source>
        <translation>Server personale</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="39"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation>URL Server personale</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="48"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="49"/>
        <source>Username (user1)</source>
        <translation>Nome utente (utente1)</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="58"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="73"/>
        <source>Device display name</source>
        <translation>Nome dispositivo mostrato</translation>
    </message>
</context>
<context>
    <name>MessageSource</name>
    <message>
        <location filename="../qml/pages/MessageSource.qml" line="16"/>
        <source>View Source</source>
        <translation>Vedi Sorgente</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../qml/pages/Messages.qml" line="38"/>
        <location filename="../qml/pages/Messages.qml" line="41"/>
        <source>File saved</source>
        <translation>File salvato</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="39"/>
        <source>File saved to Downloads directory</source>
        <translation>File salvato nella cartella degli scaricamenti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="137"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="143"/>
        <source>Reply</source>
        <translation>Rispondi</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="146"/>
        <source>Replying to </source>
        <translation>Rispondendo a</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="159"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="177"/>
        <source>View Source</source>
        <translation>Vedi Sorgente</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="182"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="276"/>
        <source>Download audio</source>
        <translation>Scarica audio</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="308"/>
        <location filename="../qml/pages/Messages.qml" line="328"/>
        <source>View video</source>
        <translation>Guarda video</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="308"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="337"/>
        <source>Download file</source>
        <translation>Scarica file</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="430"/>
        <source>Message</source>
        <translation>Messaggio</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="431"/>
        <source>Send message to room</source>
        <translation>Invia messaggio nella stanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="461"/>
        <source>New Messages</source>
        <translation>Nuovo Messaggio</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="482"/>
        <source>Send file</source>
        <translation>Invia file</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1294"/>
        <source> invited </source>
        <translation>Invitato</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1298"/>
        <source> changed their profile</source>
        <translation> hanno cambiato il loro profilo</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1300"/>
        <source> joined</source>
        <translation> è entrato</translation>
    </message>
</context>
<context>
    <name>PictureDisplay</name>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="17"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="20"/>
        <source>Image saved</source>
        <translation>Immgagine salvata</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="18"/>
        <source>Image saved to Pictures directory</source>
        <translation>Immagine salvata nella cartella Immagini</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="26"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="29"/>
        <source>Image error</source>
        <translation>Errore immagine</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="27"/>
        <source>Image not saved to Pictures directory</source>
        <translation>Immagine non salvata nella cartella Immagini</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="51"/>
        <source>Save to Pictures</source>
        <translation>Salva su Immagini</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="59"/>
        <source>Share</source>
        <translation>Condividi</translation>
    </message>
</context>
<context>
    <name>RoomDirectory</name>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="28"/>
        <source>Explore public rooms</source>
        <translation>Esplora stanze pubbliche</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="33"/>
        <source>Find a room...</source>
        <translation>Cerca una stanza...</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="46"/>
        <source>Search in</source>
        <translation>Cerca su</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="49"/>
        <source>My homeserver</source>
        <translation>Il mio server personale</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="50"/>
        <source>Matrix.org</source>
        <translation>Matrix.org</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="60"/>
        <source>No results</source>
        <translation>Nessun risultato</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="61"/>
        <source>Please try a different search term</source>
        <translation>Prova un termine diverso di ricerca</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="71"/>
        <source>Search for a room</source>
        <translation>Cerca una stanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="72"/>
        <source>Use the search box to search for a room</source>
        <translation>Usa la casella di ricerca per cercare una stanza</translation>
    </message>
</context>
<context>
    <name>Rooms</name>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="129"/>
        <source>No Favorites</source>
        <translation>Nessun Preferito</translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="145"/>
        <source>No Rooms</source>
        <translation>Nessuna Stanza</translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="161"/>
        <source>No Direct Messages</source>
        <translation>Nessun Messaggio Diretto</translation>
    </message>
    <message>
        <location filename="../qml/pages/Rooms.qml" line="176"/>
        <source>No Invites</source>
        <translation>Nessun Invito</translation>
    </message>
</context>
<context>
    <name>RoomsDisplay</name>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="42"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="47"/>
        <source>Explore public rooms</source>
        <translation>Esplora stanze pubbliche</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="53"/>
        <source>Create new room</source>
        <translation>Crea una nuova stanza</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="59"/>
        <location filename="../qml/custom/RoomsDisplay.qml" line="74"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="59"/>
        <source>Hide Search</source>
        <translation>Nascondi Ricerca</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="95"/>
        <source>Remove from Favorites</source>
        <translation>Rimuovi dai Favoriti</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="95"/>
        <source>Favorite</source>
        <translation>Preferito</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="107"/>
        <source>Leave</source>
        <translation>Esci</translation>
    </message>
</context>
<context>
    <name>SSOLogin</name>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="36"/>
        <source>Login with SSO</source>
        <translation>Accedi con SSO</translation>
    </message>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="40"/>
        <source>Homeserver</source>
        <translation>Server personale</translation>
    </message>
    <message>
        <location filename="../qml/pages/SSOLogin.qml" line="41"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation>URL Server personale (matrix.org)</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="65"/>
        <source>Enable notifications</source>
        <translation>Abilita notifiche</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="72"/>
        <source>Enable notifications when app is closed</source>
        <translation>Ailita notifiche quando l&apos;app è chiusa</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="80"/>
        <source>Notification checking interval</source>
        <translation>Intervallo di controllo notifiche</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <source>30 seconds</source>
        <translation>30 secondi</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="83"/>
        <source>2.5 minutes</source>
        <translation>2.5 minuti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="84"/>
        <source>5 minutes</source>
        <translation>5 minuti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <source>10 minutes</source>
        <translation>10 minuti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="86"/>
        <source>15 minutes</source>
        <translation>15 minuti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>30 minutes</source>
        <translation>30 minuti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>1 hour</source>
        <translation>1 ora</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="89"/>
        <source>2 hours</source>
        <translation>2 ore</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>4 hours</source>
        <translation>4 ore</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>8 hours</source>
        <translation>8 ore</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="92"/>
        <source>10 hours</source>
        <translation>10 ore</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="93"/>
        <source>12 hours</source>
        <translation>12 ore</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="94"/>
        <source>24 hours</source>
        <translation>24 ore</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="104"/>
        <source>Display</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="109"/>
        <source>Sort rooms by</source>
        <translation>Ordina stanze</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="111"/>
        <source>Activity</source>
        <translation>Attività</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Alphabetical</source>
        <translation>Alfabetico</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="122"/>
        <source>Display user avatars</source>
        <translation>Mostra avatar utenti</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="129"/>
        <source>Global</source>
        <translation>Globale</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="133"/>
        <source>Clear cache</source>
        <translation>Pulisci cache</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="134"/>
        <source>Cleared cache</source>
        <translation>Cache pulita</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="138"/>
        <source>Logout</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="140"/>
        <source>Logging out</source>
        <translation>Sto uscendo</translation>
    </message>
</context>
<context>
    <name>Share</name>
    <message>
        <location filename="../qml/pages/Share.qml" line="13"/>
        <source>Share</source>
        <translation>Condividi</translation>
    </message>
</context>
<context>
    <name>Start</name>
    <message>
        <location filename="../qml/pages/Start.qml" line="22"/>
        <source>About</source>
        <translation>A proposito</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="40"/>
        <source>Welcome</source>
        <translation>Benvenuto</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="44"/>
        <source>Log in with Username/Password</source>
        <translation>Accedi con Nome utente/Password</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="52"/>
        <source>Log in with SSO</source>
        <translation>Accedi con SSO</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../qml/pages/User.qml" line="83"/>
        <source>Direct Message</source>
        <translation>Messaggio diretto</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="94"/>
        <source>Ignore</source>
        <translation>Ignora</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="104"/>
        <source>Unignore</source>
        <translation>Non ignorare</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="17"/>
        <location filename="../qml/pages/VideoPage.qml" line="20"/>
        <source>Video saved</source>
        <translation>Video salvato</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="18"/>
        <source>Video saved to Videos directory</source>
        <translation>Video salvato nella cartella Video</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="26"/>
        <location filename="../qml/pages/VideoPage.qml" line="29"/>
        <source>Video error</source>
        <translation>Errore video</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="27"/>
        <source>Video not saved to Videos directory</source>
        <translation>Video non salvato nella cartella Video</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="57"/>
        <source>Share</source>
        <translation>Condividi</translation>
    </message>
</context>
</TS>
